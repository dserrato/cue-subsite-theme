<?php

function cuesubsite_show_logo() {
  $logo = theme_get_setting('logo');
  $html = "<img src=\"{$logo}\">";

  return $html;
}

/**
 * hook_contextual_links_view_alter
 *
 * To add contextual link for nodequeue ordering of banners
 */
function cuesubsite_contextual_links_view_alter(&$element, $items) {

  // Banners block 
  // Order view, Add banner
  if (isset($element['#element']['#block']) 
        && $element['#element']['#block']->delta == 'banners-block'
        && user_access('manipulate all queues')) {

    $element['#links']['nodequeue'] = array(
      'title' => 'Order view',
      'href' => url('admin/structure/nodequeue/1/view/1', array('absolute' => true))
    );
    $element['#links']['add_banner'] = array(
      'title' => 'Add banner',
      'href' => url('node/add/banner', array('absolute' => true))
    );
  }

  // Social network block
  // Add social network
  if (isset($element['#element']['#block']) 
        && $element['#element']['#block']->delta == 'social_network-block'
        && user_access('create social_network content')) {

    $element['#links']['add_social_network'] = array(
      'title' => 'Add social network',
      'href' => url('node/add/social-network', array('absolute' => true))
    );
  }

  // Board members page
  // Add board member
    // print_r($element['#element']['content']['system_main']['#block']);
  // if (isset($element['#element']['content'])
  //       && $element['#element']['content']['system_main']['#block']->bid == 37) {
    // $element['#links']['add_board_member'] = array(
    //   'title' => 'Add board member',
    //   'href' => url('node/add/board-members', array('absolute' => true))
    // );
  // }

}

/**
  Implement theme_preprocess_page
 */
function cuesubsite_preprocess_page(&$variables) {
  $variables['custom_menu'] = menu_navigation_links('main-menu');
}

/**
  Implement theme_links
 */
function cuesubsite_links__menu_custom_menu(&$variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    $output = '';

    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading. 
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<div' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
        && (empty($link['language']) || $link['language']->language == $language_url->language)) {
          $class[] = 'active';
        }
      // $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      // $output .= "</li>\n";
      $output .= "\n";
    }

    $output .= '</div>';
  }

  return $output;
}
