<footer>

  <div class="wrap">

    <?php  print theme('links__menu_custom_menu', array('links' => $custom_menu, 'attributes' => array('id' => 'menu-footer-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => false )); ?>

    <?php if ($page['footer']) : ?>
      <?php $page['footer'] && print render($page['footer']); ?>
    <?php endif; ?>
    <p><?php print $site_name; ?> is part of <a href="http://www.cue.org">CUE</a>, a 501©3 non-profit California corporation.</p>
    <p>For more information about <a href="http://www.cue.org">CUE</a>, visit <a href="http://www.cue.org">www.cue.org</a></p>

    <p class="copyright">Copyright &copy; <?php echo date('Y') ?>. All Rights Reserved.</p>
  </div>

</footer>

