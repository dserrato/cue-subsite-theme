
<div class="container">

  <?php require('includes/header.inc'); ?>

  <div id="main">

    <?php if ($page['sidebar_first']) : ?>
      <div id="sidebar_first">
        <?php $page['sidebar_first'] && print render($page['sidebar_first']);?>
      </div><!-- #sidebar_first -->
    <?php endif; ?>

    <div id="content">

    <?php if ($page['content_top']) : ?>
      <div id="content_top">
        <?php $page['content_top'] && print render($page['content_top']); ?>
      </div><!-- #content_top-->
    <?php endif; ?>

      <?php print render($title_prefix);?>

      <h2><?php print $title; ?></h2>
      <?php $tabs && print render($tabs); ?>

      <?php print $messages; ?>
      <?php print render($page['content']); ?>

      <?php print render($title_suffix); ?>

    <?php if ($page['content_bottom']) : ?>
      <div id="content_bottom">
        <?php $page['content_bottom'] && print render($page['content_bottom']); ?>
      </div><!-- #content_bottom -->
    <?php endif; ?>

    </div>

    <?php if ($page['sidebar_second']) : ?>
      <div id="sidebar_second">
        <?php $page['sidebar_second'] && print render($page['sidebar_second']); ?>
      </div><!-- #sidebar_second -->
    <?php endif; ?>

  </div><!-- #main -->

</div><!-- .container -->

<?php require('includes/footer.inc'); ?>

</body>
</html>
